'use strict';
const app = require('../server/server.js');
const EventEmitter = require('events');
EventEmitter.defaultMaxListeners = 5000;
const ds = app.datasources.leather;
let filteredModels = Object.keys(app.models).map(k => {
  if (app.models[k].dataSource.name == 'leather') return app.models[k].modelName;
}).filter(a=>!!a);
filteredModels = [...new Set(filteredModels)];
console.log(JSON.stringify(filteredModels));
ds.automigrate(filteredModels, err => {
  if (err) {
    throw err;
  }
  console.log('models created!');
  ds.disconnect();
  process.exit();
});
